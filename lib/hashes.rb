# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  ans = {}
  str.split.each { |word| ans[word] = word.length }
  ans
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  ordered = hash.sort_by { |k, v| v }
  ordered[-1].first
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each { |key, value| older[key] = value }
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  answer = Hash.new(0)
  letters = word.chars
  letters.each { |letter| answer[letter] += 1 }
  answer
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  answer = {}
  arr.each { |word| answer[word] = 1 }
  answer.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  answer = { even: 0, odd: 0 }
  numbers.each do |number|
    answer[:even] += 1 if number.even?
    answer[:odd] += 1 if number.odd?
  end
  answer
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  counter = Hash.new(0)
  vowels = string.chars.select { |letter| 'aeiou'.include?(letter) }
  vowels.each { |vowel| counter[vowel] += 1 }
  counter.sort_by { |k, v| v }[-1][0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  fw_students = []
  students.each { |name, bday| fw_students << name if bday > 6 }

  ans = []
  fw_students.each_index do |idx1|
    ((idx1 + 1)...fw_students.length).each do |idx2|
      ans << [fw_students[idx1], fw_students[idx2]]
    end
  end
  ans
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  animals = Hash.new(0)
  specimens.each { |animal| animals[animal] += 1 }
  animals_arr = animals.sort_by { |k, v| v }
  (animals.length**2) * animals_arr[0][1] / animals_arr[-1][1]
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_letters = Hash.new(0)
  normal_sign.downcase.chars.each { |letter| normal_letters[letter] += 1 }

  vandal_letters = Hash.new(0)
  vandalized_sign.downcase.chars.each { |letter| vandal_letters[letter] += 1 }

  vandal_letters.each do |letter, value|
    return false if normal_letters[letter] < vandal_letters[letter]
  end
  true
end
